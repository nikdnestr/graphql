import React from 'react';
import { List } from './components/List';
import { PostForm } from './components/PostForm';

const App = () => {
  return (
    <div style={styles}>
      <PostForm />
      <List />
    </div>
  );
};

const styles = {
  display: 'flex',
  flexDirection: 'column',
};

export default App;
