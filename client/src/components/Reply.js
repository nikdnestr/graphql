import React from 'react';

export const Reply = ({ content }) => {
  return <div style={styles}>{content}</div>;
};

const styles = {
  marginTop: '10px',
  padding: '10px',
  background: '#BEF090',
  borderRadius: '10px',
};
