import React, { useState } from 'react';
import { useMutation, gql } from '@apollo/client';

export const ReplyForm = ({ id }) => {
  const [input, setInput] = useState('');
  const MAKE_REPLY = gql`
    mutation makeReply($id: ID!, $content: String!) {
      makeReply(id: $id, content: $content) {
        content
      }
    }
  `;
  const [makeReply] = useMutation(MAKE_REPLY);

  const onSubmit = (e) => {
    e.preventDefault();
    makeReply({ variables: { id: id, content: input } });
  };

  return (
    <form style={styles.container} onSubmit={(e) => onSubmit(e)}>
      <textarea
        value={input}
        onChange={(e) => setInput(e.target.value)}
        style={styles.input}
      ></textarea>
      <button style={styles.button}>Respond</button>
    </form>
  );
};

const styles = {
  container: {
    width: '500px',
    display: 'flex',
    marginTop: '10px',
    // padding: '0 20px',
  },
  input: {
    width: '100%',
    height: '100%',
    boxSizing: 'border-box',
    resize: 'none',
  },
  button: {
    padding: '10px',
    height: '100%',
    marginLeft: '10px',
    background: '#0077B6',
    color: '#fff',
    border: 'none',
    borderRadius: '10px',
  },
};
