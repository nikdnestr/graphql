import { useQuery, gql } from '@apollo/client';
import React from 'react';
import { Post } from './Post';

export const List = () => {
  const ALL_POSTS = gql`
    query AllPosts {
      allPosts {
        id
        content
        replies {
          content
        }
      }
    }
  `;

  const POSTS_SUBSCRIPTION = gql`
    subscription {
      newPost {
        id
        content
        replies {
          id
          content
        }
      }
    }
  `;

  const { data, loading } = useQuery(ALL_POSTS);

  console.log(data);

  return (
    <div style={styles.contaienr}>
      {loading && <div>Loading...</div>}
      {!loading &&
        data.allPosts.map((post, index) => {
          console.log(post.replies);
          return (
            <Post
              content={post.content}
              id={post.id}
              reply={post.replies}
              key={post.id}
            />
          );
        })}
    </div>
  );
};

const styles = {};
