import React, { useState } from 'react';
import { Button } from './Button';
import { ReplyForm } from './ReplyForm';
import { Icons } from './Icons';
import { Reply } from './Reply';

export const Post = ({ content, id, reply }) => {
  const [repliesOn, setRepliesOn] = useState(false);

  const onClick = () => {
    setRepliesOn(!repliesOn);
  };

  console.log(reply);

  const getReplies = () => {
    let list = null;
    if (reply.length > 0) {
      list = reply.map((r) => {
        return <Reply content={r.content} />;
      });
    }
    return (
      <div>
        {list}
        <ReplyForm id={id} />
      </div>
    );
  };

  return (
    <div style={styles.container}>
      <div style={styles.content}>
        <div>{content}</div>
        <Button title="Reply" onClick={onClick} />
      </div>
      <div style={styles.icons}>
        <Icons id={id} />
        <p style={styles.id}>#{id.slice(id.length - 3).toUpperCase()}</p>
      </div>
      <div style={styles.reply}>{repliesOn ? getReplies() : null}</div>
    </div>
  );
};

const styles = {
  container: {
    width: '500px',
    background: '#90E0EF',
    padding: '20px',
    margin: '10px',
    borderRadius: '10px',
  },
  content: {
    marginLeft: '10px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    color: '#000',
  },
  icons: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '10px',
  },
  id: { margin: '0 0 0 auto' },
  reply: { display: 'flex', flexDirection: 'row' },
};
