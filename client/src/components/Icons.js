import React, { useState } from 'react';
import { IconContext } from 'react-icons';
import { BiDislike, BiLike } from 'react-icons/bi';

export const Icons = () => {
  const [like, setLike] = useState(false);
  const [dislike, setDislike] = useState(false);

  const onLike = () => {
    setDislike(false);
    setLike(!like);
  };

  const onDislike = () => {
    setLike(false);
    setDislike(!dislike);
  };

  return (
    <div style={styles.container}>
      <div onClick={() => onLike()}>
        <IconContext.Provider
          value={{ size: '20px', color: like ? '#0077B6' : 'black' }}
        >
          <BiLike />
        </IconContext.Provider>
      </div>
      <div onClick={() => onDislike()}>
        <IconContext.Provider
          value={{ size: '20px', color: dislike ? '#0077B6' : 'black' }}
        >
          <BiDislike />
        </IconContext.Provider>
      </div>
      <div style={styles.value}></div>
    </div>
  );
};

const styles = {
  container: { marginRight: '10px', display: 'flex', flexDirection: 'row' },
  value: { marginBottom: '2px' },
};
