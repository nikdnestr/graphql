import React from 'react';

export const Button = ({ title, onClick }) => {
  return (
    <div>
      <button onClick={onClick} style={styles}>
        {title}
      </button>
    </div>
  );
};

const styles = {
  padding: '5px 10px',
  background: '#0077B6',
  color: '#fff',
  border: 'none',
  borderRadius: '10px',
};
