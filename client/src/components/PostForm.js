import React, { useState } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';

const ALL_POSTS = gql`
  query {
    allPosts {
      id
      content
    }
  }
`;

const MAKE_POST = gql`
  mutation makePost($content: String!) {
    makePost(content: $content) {
      content
    }
  }
`;

export const PostForm = () => {
  const [input, setInput] = useState('');

  const { data, loading } = useQuery(ALL_POSTS);
  const [makePost] = useMutation(MAKE_POST);

  return (
    <form
      style={styles.container}
      onSubmit={(e) => {
        makePost({ variables: { content: input } });
        console.log(data);
        e.preventDefault();
        setInput('');
      }}
    >
      <textarea
        style={styles.input}
        value={input}
        onChange={(e) => setInput(e.target.value)}
      ></textarea>
      <button type="submit">post</button>
    </form>
  );
};

const styles = {
  container: {
    display: 'flex',
  },
  input: {
    width: '100%',
    height: '100%',
    boxSizing: 'border-box',
    resize: 'none',
  },
};
