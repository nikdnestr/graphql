function makePost(parent, args, context, info) {
  return context.prisma.createPost({
    content: args.content,
    likes: 0,
    dislikes: 0,
  });
}

async function makeReply(parent, args, context, info) {
  const exists = await context.prisma.$exists.post({
    id: args.id,
  });

  if (!exists) {
    throw new Error(`Post with ID ${args.id} does not exist`);
  }

  return context.prisma.createReply({
    content: args.content,
    post: { connect: { id: args.id } },
  });
}

async function likePost(parent, args, context, info) {
  return context.prisma.updatePost({
    increment: 1,
  });
}

async function dislikePost(parent, args, context, info) {
  return context.prisma.updatePost({
    likes: {
      decrement: 1,
    },
  });
}

module.exports = {
  makePost,
  makeReply,
  likePost,
  dislikePost,
};
