async function allPosts(parent, args, context) {
  const posts = await context.prisma.posts();
  return posts;
}

async function getPostById(parent, args, context) {
  const post = await context.prisma.post({
    id: args.id,
  });
  return post;
}

module.exports = {
  allPosts,
  getPostById,
};
