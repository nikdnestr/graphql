function post(parent, args, context) {
  return context.prisma
    .replyPost({
      id: parent.id,
    })
    .post();
}

module.exports = {
  post,
};
